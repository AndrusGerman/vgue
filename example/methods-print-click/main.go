package main

import (
	"fmt"
	"syscall/js"

	vue "gitlab.com/AndrusGerman/vgue"
)

func main() {
	// Create new Vue App
	app := vue.Vue{
		El: "#app",
		Primitive: vue.Primitive{
			Methods: map[string]js.Func{
				"printclick": js.FuncOf(func(this js.Value, ig []js.Value) interface{} {
					fmt.Println("Click")
					count := this.Get("count")
					this.Set("count", count.Int()+1)
					return nil
				}),
			},
			Data: map[string]interface{}{
				"count": 1,
			},
			Watch: map[string]js.Func{
				"count": js.FuncOf(func(this js.Value, arg []js.Value) interface{} {
					fmt.Println("NewVal: ", arg[0].Int())
					fmt.Println("OldVal: ", arg[0].Int())
					return nil
				}),
			},
		},
	}

	// Run App
	app.StartWait()
}
