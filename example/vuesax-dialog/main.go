package main

import (
	"syscall/js"

	vue "gitlab.com/AndrusGerman/vgue"
)

func main() {
	// Create new Vue App
	app := vue.Vue{
		El: "#vuesax-params",
		Primitive: vue.Primitive{
			Data: map[string]interface{}{
				"colorAlert": "primary",
			},
			Methods: map[string]js.Func{
				// Open Alert Function
				"openAlert": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
					this.Get("$vs").Call("dialog", js.ValueOf(map[string]interface{}{
						"color":  this.Get("colorAlert"),
						"title":  "Alerta Color",
						"text":   "Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
						"accept": this.Get("acceptAlert"), // accept function
					}))
					return nil
				}),
				// Open accept Function
				"acceptAlert": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
					this.Get("$vs").Call("notify", js.ValueOf(map[string]interface{}{
						"color": this.Get("colorAlert"),
						"title": "Accept Selected",
						"text":  "Thanks for accept.",
					}))
					return nil
				}),
			},
		},
	}
	// Run App
	app.StartWait()
}
