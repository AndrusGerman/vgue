package main

import (
	vue "gitlab.com/AndrusGerman/vgue"
	router "gitlab.com/AndrusGerman/vgue/vuegorouter"
)

func main() {
	Foo := vue.Component{
		Template: "<div>foo</div>",
	}
	Bar := vue.Component{
		Template: "<div>bar</div>",
	}

	routes := []router.Route{
		{Path: "/foo", Component: &Foo},
		{Path: "/bar", Component: &Bar},
	}
	// Create VueRouter
	r := router.VueRouter{
		Routes:    routes,
		Component: &vue.Component{},
	}
	r.MountWait("#app")
}
