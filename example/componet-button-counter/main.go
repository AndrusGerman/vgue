package main

import (
	vue "gitlab.com/AndrusGerman/vgue"
)

func main() {
	// Create new Vue App
	app := vue.Vue{El: "#app"}
	// Create Component
	componet := vue.Component{
		Template: `<button v-on:click="count++">You clicked me {{ count }} times.</button>`,
		Primitive: vue.Primitive{
			Data: map[string]interface{}{
				"count": 0,
			},
		},
	}

	componet.Set("button-counter")

	// Run App
	app.StartWait()
}
