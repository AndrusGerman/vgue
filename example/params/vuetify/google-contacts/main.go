package main

import (
	"syscall/js"

	vue "gitlab.com/AndrusGerman/vgue"
)

func main() {
	// Create new Vue App
	app := vue.Vue{
		El: "#app-contacts",
		Primitive: vue.Primitive{
			Methods: map[string]js.Func{
				// Save User Function
				"SaveUser": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
					this.Set("dialog", false)
					user := this.Get("user")
					message := "Nombre: " + user.Get("name").String() + ", Numero: " + user.Get("number").String()
					js.Global().Call("alert", message) // Alert Message
					// Clear Var
					user.Set("name", "")
					user.Set("number", "")
					return nil
				}),
			},
			Data: map[string]interface{}{
				"dialog": false,
				"source": "https://gitlab.com/AndrusGerman/vgue/tree/master/example/params/vuetify/google-contacts",
				"drawer": nil,
				"items": []interface{}{
					map[string]interface{}{"icon": "contacts", "text": "Contacts"},
					map[string]interface{}{"icon": "history", "text": "Frequently contacted"},
					map[string]interface{}{"icon": "content_copy", "text": "Duplicates"},
					map[string]interface{}{
						"icon":     "keyboard_arrow_up",
						"icon-alt": "keyboard_arrow_down",
						"text":     "Labels",
						"model":    true,
						"children": []interface{}{
							map[string]interface{}{"icon": "add", "text": "Create label"},
						},
					},
				},
				"user": map[string]interface{}{
					"name":   "",
					"number": "",
				},
			},
		},
	}
	// Run App
	app.StartWait(vue.Params{Name: "vuetify", Value: js.Global().Get("Vuetify").New()})
}
