package vgue

import (
	"syscall/js"

	"gitlab.com/AndrusGerman/vgue/vuegotools"
)

// Primitive Model, Basic structure of any vue model
type Primitive struct {
	// Data: Vars for vue
	Data map[string]interface{}
	// Methods: Methods Vue
	Methods map[string]js.Func
	// Watch: Watch functions
	Watch map[string]js.Func
	// Created: func call on created componet
	Created func(this js.Value)
	// Params: Custom params in newVue Object
	Params []Params
}

func (ctx *Primitive) getParseData(vparams ...Params) js.Value {
	vparams = append(vparams, ctx.Params...)
	// Create Basic Dt
	var dat = vuegotools.Obj.New()
	// Set Vars
	if ctx.Data != nil {
		dat.Set("data", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			return ctx.Data
		}))
	}
	// Set Func created
	if ctx.Created != nil {
		dat.Set("created", js.FuncOf(func(this js.Value, arg []js.Value) interface{} {
			ctx.Created(this)
			return nil
		}))
	}
	// Set Basic Methods
	if ctx.Methods != nil {
		methods := vuegotools.Obj.New()
		for ind := range ctx.Methods {
			methods.Set(ind, ctx.Methods[ind])
		}
		dat.Set("methods", methods)
	}
	// Set Basic Watch
	if ctx.Watch != nil {
		watch := vuegotools.Obj.New()
		for ind := range ctx.Watch {
			watch.Set(ind, ctx.Watch[ind])
		}
		dat.Set("watch", watch)
	}
	for ind := range vparams {
		dat.Set(vparams[ind].Name, vparams[ind].Value)
	}
	return dat
}
