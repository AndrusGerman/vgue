package vuegorouter

import (
	"syscall/js"

	vgue "gitlab.com/AndrusGerman/vgue"
	"gitlab.com/AndrusGerman/vgue/vuegotools"
)

// Route model
type Route struct {
	// Path : Example '/home'
	Path string
	// Component : Component for route
	Component *vgue.Component
}

// VueRouter Group Routes
type VueRouter struct {
	// Routes : all routes
	Routes []Route
	// Mode : Example 'history'
	Mode string
	// Component : Component main for routes
	Component *vgue.Component
}

// Mount Vue Component
func (ctx *VueRouter) Mount(el string, vparams ...vgue.Params) (js.Value, error) {
	// Get Routesredundancy array
	var routesJSArray = make([]interface{}, len(ctx.Routes))
	for ind := range ctx.Routes {
		routesJSArray[ind] = map[string]interface{}{
			"path":      ctx.Routes[ind].Path,
			"component": ctx.Routes[ind].Component.GetDataObject(),
		}
	}
	// Create Routes Object
	routesObject := vuegotools.Obj.New()
	routesObject.Set("routes", js.ValueOf(routesJSArray))
	if ctx.Mode != "" {
		routesObject.Set("mode", ctx.Mode)
	}

	// Create VueRouter
	routeNewRouteVue := js.Global().Get("VueRouter").New(routesObject)
	// Append new params
	vparams = append(vparams, vgue.Params{Name: "router", Value: routeNewRouteVue})

	// Get Vue
	vg, err := vuegotools.GetVue()
	if err != nil {
		return js.Undefined(), err
	}
	// Set In Vue
	obj := ctx.Component.GetDataObject(vparams...)
	return vg.New(obj).Call("$mount", el), nil
}

// MountWait : Mount vue and wait
func (ctx *VueRouter) MountWait(el string, vparams ...vgue.Params) error {
	var canal = make(chan error)
	go func() {
		_, err := ctx.Mount(el, vparams...)
		if err != nil {
			canal <- err
		}
	}()
	return <-canal
}
