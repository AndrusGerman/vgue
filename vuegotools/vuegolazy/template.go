package vuegolazy

import (
	"io"
	"net/http"
)

func GetContent(name string) func() []byte {
	return func() []byte {
		resp, err := http.Get(name)
		if err != nil {
			println("vuegotools: get "+name+" error ", err)
			return make([]byte, 0)
		}

		defer resp.Body.Close()
		bt, err := io.ReadAll(resp.Body)
		if err != nil {
			println("vuegotools: read "+name+" error ", err)
			return make([]byte, 0)
		}
		return bt
	}
}
