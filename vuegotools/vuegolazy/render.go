package vuegolazy

import (
	"strings"
	"syscall/js"

	"gitlab.com/AndrusGerman/vgue"
)

// Create Custom NewRender, for lazyLoading html content
// Using http can greatly increase the file size,
// if possible use local imports instead of LazyLoading
func NewRender(templatePath string, component *vgue.Component) *vgue.Component {
	var primera = 0
	var name = replacer(templatePath) + "-generated"
	var nuevo = &vgue.Component{
		Primitive: vgue.Primitive{Params: []vgue.Params{{Name: "render", Value: js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			if primera == 0 {
				go func() {
					component.Template = string(GetContent(templatePath)())
					component.Set(name)
					primera++
					this.Call("$forceUpdate")
				}()
				return ""
			}
			return args[0].Invoke(name)
		})}}},
	}
	return nuevo
}

func replacer(texto string) (result string) {
	for _, letter := range texto {
		var upper = strings.ToUpper(string(letter))
		var toLower = strings.ToLower(string(letter))
		if upper != toLower {
			result += string(letter)
		}
	}
	return strings.ToLower(result)
}
