package vuegotools

import (
	"errors"
	"syscall/js"
)

// GetVue short
func GetVue() (js.Value, error) {
	value := js.Global().Get("Vue")
	if value.IsUndefined() {
		return value, errors.New("vue: Is Undefined")
	}
	return value, nil
}

func VueUse(args ...interface{}) error {
	vu, err := GetVue()
	if err != nil {
		return err
	}
	vu.Call("use", args...)
	return nil
}
