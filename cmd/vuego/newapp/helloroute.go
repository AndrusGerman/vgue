package newapp

import (
	"log"
	"os"

	"gitlab.com/AndrusGerman/vgue/cmd/vuego/build"
)

func helloRoute(folder string) {
	err := os.Mkdir(folder, 0777)
	if err != nil {
		log.Println("Error create p folder: ", err)
		os.Exit(1)
	}
	file, err := os.OpenFile(folder+"/main.go", os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Println("Error main file: ", err)
		os.Exit(1)
	}
	defer file.Close()
	_, err = file.WriteAt([]byte(`package main

import (
	vue "gitlab.com/AndrusGerman/vgue"
	router "gitlab.com/AndrusGerman/vgue/vuegorouter"
)

func main() {
	// Routes Buttons Component
	routesButtons := vue.Component{Template: `+"`"+`<div><p>Routes  Buttons: </p>
	<router-link to="/">Go to Home</router-link>
	<router-link to="/saludo">Go to Saludo</router-link>
	<router-link to="/cualquiercosa">Go to 404</router-link></div>`+"`"+`}
	routesButtons.Set("routes-buttons")
	// Create new Vue main
	main := &vue.Component{
		Template: "<router-view></router-view>",
	}
	routes := []router.Route{
		// Route main Component Main
		{Path: "/", Component: &vue.Component{
			Template: "<div><h1>Home</h1><routes-buttons></routes-buttons></div>",
		}},
		{Path: "/saludo", Component: &vue.Component{
			Template: "<div><h1>Saludo</h1><routes-buttons></routes-buttons></div>",
		}},
		{Path: "**", Component: &vue.Component{
			Template: "<div><h1>404</h1><routes-buttons></routes-buttons></div>",
		}},
	}
	r := router.VueRouter{Component: main, Routes: routes, Mode: "history"}
	r.MountWait("#app")
}
`), 0)
	if err == nil {
		log.Println("main.go: File generated successfully")
	}
	generateModule(folder)
	generateIndex(folder, ``)
	build.GetModule(folder)
	build.Build(folder)
}
