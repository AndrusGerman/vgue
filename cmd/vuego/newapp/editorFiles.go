package newapp

import (
	"log"
	"os"
)

func generateEditorFiles(folder string) {
	html := []byte(`{
		"go.toolsEnvVars": {
			"GOOS":"js",
			"GOARCH":"wasm",
		}
	}`)
	if err := os.Mkdir(folder+"/.vscode", 0777); err != nil {
		log.Println("Error: Create editor folder: ", err)
		return
	}
	file, err := os.OpenFile(folder+"/.vscode/settings.json", os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Println("Error: Create editor file: ", err)
		return
	}
	defer file.Close()
	file.WriteAt(html, 0)
	log.Println("settings.json: File generated successfully")
}
