package newapp

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgoflags"
)

func generateModule(nameProyecto string) {
	cmd := exec.Command("go", "mod", "init", nameProyecto)
	cmd.Dir = nameProyecto
	cmd.Run()
}

func generateIndex(folder string, content string) {
	html := []byte(`
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>` + folder + `</title>
</head>

<body>
	<script src="` + LibVue.Get(folder, "scripts", "vue.js") + `"></script>
	<script src="` + LibVueRoute.Get(folder, "scripts", "vue-router.js") + `"></script>
	<script src="./scripts/wasm_exec.js"></script>
	<div id="app">
` + content + `
	</div>
	<script>
		const go = new Go();
		WebAssembly.instantiateStreaming(fetch('./main.wasm'), go.importObject).then(v => go.run(v.instance));
	</script>
</body>
</html>`)

	file, err := os.OpenFile(folder+"/index.html", os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Println("Error main file: ", err)
		os.Exit(1)
	}
	defer file.Close()
	file.WriteAt(html, 0)
}

func whatIDo(folder string) {
	fmt.Println("VGUE: exec 🤔")
	fmt.Println("$ cd " + folder)
	fmt.Println("$ vuego start")
}

func DownloadLib() bool {
	return vgoflags.Get("-d")
}
