package newapp

import (
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/libs"
)

type Libs libs.Libs

var LibVue Libs = "https://unpkg.com/vue/dist/vue.js"
var LibVueRoute Libs = "https://unpkg.com/vue-router/dist/vue-router.js"

func (lb Libs) Get(AppFolder string, scriptsFolder string, NameLibs string) string {
	if !DownloadLib() {
		return string(lb)
	}
	return libs.Libs(lb).Download(AppFolder, scriptsFolder, NameLibs)
}
