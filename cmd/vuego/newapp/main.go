package newapp

import (
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/wasm"
)

// Start hello
func Start(folder string) {
	if folder == "" {
		folder = "hello-route"
	}
	helloRoute(folder)
	wasm.Start(folder)
	generateEditorFiles(folder)
	whatIDo(folder)
}
