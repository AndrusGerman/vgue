package server

import (
	"log"
	"net/http"
	"os"
	"strconv"
)

var port = "3001"

var folder = "./"

// Start Server
func Start(flag string) {
	// Get ENV port
	if p := os.Getenv("PORT"); p != "" {
		port = p
	}
	//Convert in int
	number, err := strconv.Atoi(port)
	if err != nil {
		log.Println("Error start server: ", err)
		return
	}
	// Index exist?
	_, err = os.Stat("./index.html")
	if err != nil {
		log.Println("Error index.html not exist ")
		return
	}

	http.HandleFunc("/", server)
	for {
		log.Println("server: Listen in http://localhost:" + strconv.Itoa(number))
		err = http.ListenAndServe(":"+strconv.Itoa(number), nil)
		if err != nil {
			log.Println("Error start server: ", err)
		}
		number++
	}
}

// server
func server(w http.ResponseWriter, r *http.Request) {
	if _, err := os.Stat(folder + r.URL.Path); err != nil {
		http.ServeFile(w, r, folder+"/index.html")
		return
	}
	http.ServeFile(w, r, folder+r.URL.Path)
}
