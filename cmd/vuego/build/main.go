package build

import (
	"log"
	"os"
	"os/exec"

	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgoflags"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgowatch"
)

func BuildNow() {
	// Build with watch files
	if IsWatch() {
		go vgowatch.Start(func() {
			initbuild("")
		})
		return
	}
	// Normal build
	Build("")
}

// Normal Build
func Build(folder string) {
	if err := initbuild(folder); err != nil {
		os.Exit(1)
	}
}

func initbuild(folder string) error {
	log.Println("build: Building...")
	os.Setenv("GOARCH", "wasm")
	os.Setenv("GOOS", "js")
	cmd := exec.Command("go", "build", "-o", "main.wasm")
	cmd.Dir = folder
	ou, err := cmd.Output()
	if err != nil {
		log.Println("build: Error build", err, cmd.Args, ","+string(ou))
		return err
	}
	log.Println("build: Building complete ~ OK")
	return nil
}

// GetModule build
func GetModule(folder string) {
	os.Setenv("GOARCH", "wasm")
	os.Setenv("GOOS", "js")
	cmd := exec.Command("go", "get", ".")
	cmd.Dir = folder
	ou, err := cmd.Output()
	if err != nil {
		log.Println("Error get modules", err, cmd.Args, ","+string(ou))
	}
}

// Find glag -w
func IsWatch() bool {
	return vgoflags.Get("-w")
}
