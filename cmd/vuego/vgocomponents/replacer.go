package vgocomponents

import (
	"io/ioutil"
	"os"
	"path"
	"strings"
)

var camelSeparator = "_"

type replacer string

func (value replacer) getParse(nameComponent string) string {
	var upperNormalPublic = getUpper(nameComponent)
	var camelNormal = getCamel(upperNormalPublic)
	var upperNormalPrivate = upperNormalPublic
	upperNormalPrivate = strings.ToLower(upperNormalPrivate[0:1]) + upperNormalPrivate[1:]

	// Set Names values
	rp := strings.NewReplacer(
		"home.html", camelNormal+".html",
		`"/home"`, `"/`+camelNormal+`"`,
		"home", upperNormalPrivate,
		"Home", upperNormalPublic,
	)
	return rp.Replace(string(value))
}

func (value replacer) writer(folderCreate, nameComponent string, extc string) error {
	if err := os.MkdirAll(folderCreate, 0777); err != nil {
		return err
	}

	file := path.Join(folderCreate, upperToCamel(nameComponent, camelSeparator))
	file += extc
	return ioutil.WriteFile(file, []byte(value.getParse(nameComponent)), 0777)
}

func camelToUpper(text string, union string) (result string) {
	for _, v := range strings.Split(text, union) {
		if len(v) >= 1 {
			result += strings.ToUpper(v[0:1]) + v[1:]
		}
	}
	return result
}

func upperToCamel(text string, union string) string {
	var parse string
	for ind, v := range text {
		var up = strings.ToUpper(string(v))
		var lower = strings.ToLower(string(v))
		// Is Letter Upper
		if (up == string(v) && lower != up) && ind != 0 {
			parse += union + string(v)
			continue
		}
		// First Element
		if up == string(v) && lower != up {
			parse += string(v)
			continue
		}
		// Is Letter Lower
		if lower == string(v) && lower != up {
			parse += string(v)
			continue
		}
	}
	return strings.ToLower(parse)
}

func getUpper(text string) string {
	return camelToUpper(camelToUpper(text, "-"), camelSeparator)
}

func getCamel(text string) string {
	var tx = upperToCamel(text, camelSeparator)
	return tx
}
