package vgocomponents

import (
	"log"
	"os"
)

// Start hello
func Start(folder string) {
	if folder == "" {
		log.Println("vgcomponents: Error file name is null ")
		os.Exit(1)
	}
	// Generate route.go code
	if err := routeFileBase.writer("components", getUpper(folder), "-route.go"); err != nil {
		log.Println("vgcomponents: Error generate route file", err)
		os.Exit(1)
	}
	// Generate data.go code
	if err := dataFileBase.writer("components", getUpper(folder), "-data.go"); err != nil {
		log.Println("vgcomponents: Error generate route file", err)
		os.Exit(1)
	}
	// Generate template.html code
	if err := templateFileBase.writer("templates", getUpper(folder), ".html"); err != nil {
		log.Println("vgcomponents: Error generate route file", err)
		os.Exit(1)
	}
	log.Println("vgcomponents: Generate " + getUpper(folder) + " components, complete 😳")
}
