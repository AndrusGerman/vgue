package vgocomponents

const dataFileBase replacer = `package components

import "syscall/js"

var homeData = map[string]interface{}{
	"name": "home",
}

var homeMethods = map[string]js.Func{
	"clickAlert": js.FuncOf(homeHello),
}

func homeHello(this js.Value, args []js.Value) interface{} {
	name := this.Get("name").String()
	return js.Global().Call("alert", "Hello From "+name)
}

`
