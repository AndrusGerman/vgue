package vgocomponents

const routeFileBase replacer = `package components

import (
	vue "gitlab.com/AndrusGerman/vgue"
	router "gitlab.com/AndrusGerman/vgue/vuegorouter"
	"gitlab.com/AndrusGerman/vgue/vuegotools/vuegolazy"
)

var homeComponent = &vue.Component{
	Primitive: vue.Primitive{
		Methods: homeMethods,
		Data:    homeData,
	},
}

var HomeRoute = router.Route{
	Path:      "/home",
	Component: vuegolazy.NewRender("./templates/home.html", homeComponent),
}
`
