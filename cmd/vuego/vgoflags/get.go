package vgoflags

import (
	"flag"
)

func Get(flagTag string) bool {
	for _, v := range flag.Args() {
		if v == flagTag {
			return true
		}
	}
	return false
}

func GetPlain(ind int) string {
	for _, v := range flag.Args()[ind:] {
		if v[0:1] != "-" {
			return v
		}
	}
	return ""
}
