package wasm

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
)

var wasmFileName = "wasm_exec.js"

// Start wasm_exec file generation
func Start(folder string) {
	// Get env information
	salida, err := exec.Command("go", "env", "GOROOT").Output()
	if err != nil {
		log.Println("Error get GOROOT: ", err)
		os.Exit(1)
	}

	goroot := string(salida[:len(salida)-1])

	// Get content wasmFile
	wasmText, err := ioutil.ReadFile(goroot + "/misc/wasm/" + wasmFileName)
	if err != nil {
		log.Println("Error read wasm file: ", err)
		os.Exit(1)
	}
	wasmFileName = folder + "/scripts/" + wasmFileName
	if err := os.MkdirAll(path.Dir(wasmFileName), 0777); err != nil {
		log.Println("wasm: Error create wasm folder ", err)
		os.Exit(1)
	}

	// create wasmFile
	file, err := os.OpenFile(wasmFileName, os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Println("Error generate file: ", err)
		os.Exit(1)
	}
	defer file.Close()

	// Write wasmFile
	_, err = file.WriteAt(wasmText, 0)
	if err == nil {
		log.Println("wasm_exec.js: File generated successfully")
	}
}
