package libs

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func addHTML(lib string) {
	if strings.Contains(lib, ".js") {
		addHTMLJS(lib)
	}
	if strings.Contains(lib, ".css") {
		addHTMLCSS(lib)
	}
}

func addHTMLJS(lib string) {
	bts, err := ioutil.ReadFile("./index.html")

	if err != nil {
		log.Println("Libs: Error open 'index.html'", err)
		os.Exit(1)
	}
	var strAdd = `<script src="` + lib + `"></script>`

	if bytes.Contains(bts, []byte(strAdd)) {
		log.Println("Libs: This lib is alredy add", err)
		return
	}
	resp := bytes.Replace(
		bts, []byte(`<script src="./scripts/wasm_exec.js"></script>`),
		[]byte(strAdd+`
	<script src="./scripts/wasm_exec.js"></script>`),
		1,
	)
	err = ioutil.WriteFile("./index.html", resp, 0777)
	if err != nil {
		log.Println("Libs: Error write 'index.html'", err)
		os.Exit(1)
	}
}

func addHTMLCSS(lib string) {
	bts, err := ioutil.ReadFile("./index.html")

	if err != nil {
		log.Println("Libs: Error open 'index.html'", err)
		os.Exit(1)
	}
	var strAdd = `<link href="` + lib + `" rel="stylesheet">`

	if bytes.Contains(bts, []byte(strAdd)) {
		log.Println("Libs: This lib is alredy add", err)
		return
	}
	resp := bytes.Replace(
		bts, []byte(`</head>`),
		[]byte(`	`+strAdd+`
</head>`),
		1,
	)
	err = ioutil.WriteFile("./index.html", resp, 0777)
	if err != nil {
		log.Println("Libs: Error write 'index.html'", err)
		os.Exit(1)
	}
}
