package libs

import (
	"log"
	"os"
	"strings"

	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgoflags"
)

// Start build
func Start(lib string) {
	var lb = Libs(lib)
	folder, err := os.Getwd()
	if err != nil {
		log.Println("Libs: Error get wd folder")
		os.Exit(1)
	}
	var scriptDir = lb.Download(folder, folderSave(lb.GetFileName()), lb.GetFileName())
	if addInHTML() {
		addHTML(scriptDir)
	}
}

func addInHTML() bool {
	return vgoflags.Get("-add")
}

func folderSave(file string) string {
	if strings.Contains(file, ".js") {
		return "scripts"
	}
	if strings.Contains(file, ".css") {
		return "css"
	}
	return libFlag()
}

func libFlag() string {
	if vgoflags.Get("-css") {
		return "css"
	}
	if vgoflags.Get("-js") {
		return "js"
	}
	log.Println("Libs: Error get file is not valid")
	log.Println("Libs: force type witch '-css' '-js'")
	os.Exit(1)
	return ""
}
