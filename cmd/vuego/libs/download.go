package libs

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
)

type Libs string

func (lb Libs) Download(AppFolder string, scriptsFolder string, NameLibs string) string {
	if lb.Str() == "" {
		log.Println("Libs: Error get lib, name is null")
		os.Exit(1)
	}

	// Get response
	fmt.Print("Libs: Downloading '" + lb.Str() + "'")
	resp, err := http.Get(lb.Str())
	if err != nil {
		log.Println("\nLibs: Error get lib "+lb.Str(), err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	// Read response
	libContent, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("\nLibs: Error read lib "+lb.Str(), err)
		os.Exit(1)
	}

	// Create lib folder
	scriptsFull := path.Join(AppFolder, scriptsFolder)
	if err := os.MkdirAll(scriptsFull, 0777); err != nil {
		log.Println("\nLibs: Error create lib folder", err)
		os.Exit(1)
	}

	// Write lib
	scriptFullDir := path.Join(scriptsFull, NameLibs)
	if err := ioutil.WriteFile(scriptFullDir, libContent, 0777); err != nil {
		log.Println("\nLibs: Error write lib", err)
		os.Exit(1)
	}
	fmt.Println(" - OK")
	return "./" + path.Join(scriptsFolder, NameLibs)
}

func (lb Libs) GetFileName() string {
	url, err := url.Parse(string(lb))
	if err != nil {
		log.Println("Libs: Parse url error ", err)
		os.Exit(1)
	}

	splcontent := strings.Split(url.Path, "/")

	return splcontent[len(splcontent)-1]
}

func (lb Libs) Str() string {
	return string(lb)
}
