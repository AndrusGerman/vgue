package vgowatch

import (
	"io/fs"
	"log"
	"os"
	"path"
	"strings"
)

func Start(calback func()) {
	addFiles()
	var canal = make(chan bool)
	go func() {
		forChanges(canal)
	}()
	for {
		calback()
		<-canal
	}
}

func addWatchFile(fi fs.DirEntry, dir string) {
	name := path.Join(dir, fi.Name())
	fw, err := NewFilesWatch(name)
	if err != nil {
		log.Println("watch: Error watch " + name)
		return
	}
	watchFiles = append(watchFiles, fw)
}

func getFileRecursive(dir string, ext string, addCalback func(fs.DirEntry, string)) {
	if ignoreFolderToWatchFullPath(dir) {
		return
	}
	files, _ := os.ReadDir(dir)
	SaveFolder(dir, ext)
	for _, file := range files {
		if !file.IsDir() && strings.Contains(file.Name(), ext) {
			addCalback(file, dir)
		} else if file.IsDir() {
			getFileRecursive(path.Join(dir, file.Name()), ext, addCalback)
		}
	}
}

func addFiles() {
	log.Println("vgowatch: scanfiles...")
	wd, _ := os.Getwd()
	getFileRecursive(wd, ".go", addWatchFile)
}
