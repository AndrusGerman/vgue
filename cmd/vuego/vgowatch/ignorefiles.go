package vgowatch

import "strings"

func ignoreFolderToWatchFullPath(folder string) bool {
	for _, v := range getFolderToIgnore() {
		if strings.Contains(folder, "/"+v+"/") {
			return true
		}
	}
	return false
}

func ignoreFolderToWatchName(folder string) bool {
	for _, v := range getFolderToIgnore() {
		if v == folder {
			return true
		}
	}
	return false
}

func getFolderToIgnore() []string {
	return []string{
		".git",
		"node_modules",
		"dist",
		"www",
	}
}
