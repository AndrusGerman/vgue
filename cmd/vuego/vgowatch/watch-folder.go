package vgowatch

import (
	"log"
	"os"
	"strings"
)

var watchFolder []*FolderWatch

func SaveFolder(folder string, ext string) {
	// Verifica si exite
	for _, v := range watchFolder {
		if v.Folder == folder {
			return
		}
	}

	// Lee los datos
	folderStruct, err := NewFolderWatch(folder, ext)
	if err != nil {
		log.Println("watch: Error add folder ", folder)
		return
	}

	// Agrega a la lista
	watchFolder = append(watchFolder, folderStruct)
}

func IsChangeFolders(ext string) int {
	var cantidad = 0
	for _, v := range watchFolder {
		cv, err := v.IsChange(ext)
		if err != nil {
			cantidad++
			continue
		}
		if cv {
			cantidad++
			continue
		}
	}
	return cantidad
}

type FolderWatch struct {
	Folder       string
	FilesCount   int
	FoldersCount int
}

func (ctx *FolderWatch) IsChange(ext string) (bool, error) {
	var changeCount = false
	countFiles, err := ctx.GetCountFiles(ext)
	if err != nil {
		return false, err
	}
	if countFiles != ctx.FilesCount {
		ctx.FilesCount = countFiles
		changeCount = true
	}
	countFolders, err := ctx.GetCountFolders()
	if err != nil {
		return false, err
	}
	if countFolders != ctx.FoldersCount {
		ctx.FoldersCount = countFolders
		changeCount = true
	}
	return changeCount, nil
}

func (ctx *FolderWatch) GetCountFiles(ext string) (int, error) {
	files, err := os.ReadDir(ctx.Folder)
	if err != nil {
		return 0, err
	}

	var count = 0
	for _, v := range files {
		if !v.IsDir() && strings.Contains(v.Name(), ext) {
			count++
		}
	}
	return count, nil
}

func (ctx *FolderWatch) GetCountFolders() (int, error) {
	files, err := os.ReadDir(ctx.Folder)
	if err != nil {
		return 0, err
	}

	var count = 0
	for _, v := range files {
		if v.IsDir() && !ignoreFolderToWatchName(v.Name()) {
			count++
		}
	}
	return count, nil
}

func (ctx *FolderWatch) init(folder string, ext string) (err error) {
	ctx.Folder = folder
	count, err := ctx.GetCountFiles(ext)
	if err != nil {
		return err
	}
	ctx.FilesCount = count

	countFolders, err := ctx.GetCountFolders()
	if err != nil {
		return err
	}
	ctx.FoldersCount = countFolders
	return err
}

func NewFolderWatch(folder string, ext string) (*FolderWatch, error) {
	var fw = new(FolderWatch)
	return fw, fw.init(folder, ext)
}
