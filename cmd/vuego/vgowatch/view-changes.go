package vgowatch

import (
	"log"
	"time"
)

func forChanges(canal chan bool) {
	for {
		var filesChange int
		var rescanNew bool
		var build = false
		for _, v := range watchFiles {
			cv, err := v.IsChange()
			if err != nil {
				filesChange++
				rescanNew = true
				log.Println("vgowatch: ", v.File)
				continue
			}
			if cv {
				filesChange++
				log.Println("vgowatch: ", v.File)
			}
		}

		if filesChange != 0 {
			log.Println("vgowatch: change ", filesChange, "files")
			build = true
		}
		var folderChange = IsChangeFolders(".go")

		if folderChange != 0 {
			log.Println("vgowatch: folder changes ", folderChange)
			build = true
			rescanNew = true
		}
		if build {
			canal <- build
		}

		if rescanNew {
			watchFiles = []*FilesWatch{}
			watchFolder = []*FolderWatch{}
			addFiles()
		}
		time.Sleep(1 * time.Second)
	}
}
