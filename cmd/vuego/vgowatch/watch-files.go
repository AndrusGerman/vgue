package vgowatch

import (
	"io/fs"
	"os"
)

var watchFiles []*FilesWatch

type FilesWatch struct {
	stats fs.FileInfo
	File  string
}

func (ctx *FilesWatch) IsChange() (bool, error) {
	stat, err := os.Stat(ctx.File)
	if err != nil {
		return false, err
	}

	if stat.Size() != ctx.stats.Size() || stat.ModTime() != ctx.stats.ModTime() {
		ctx.stats = stat
		return true, nil
	}
	return false, nil

}

func (ctx *FilesWatch) init(file string) (err error) {
	ctx.stats, err = os.Stat(file)
	ctx.File = file
	return err
}

func NewFilesWatch(file string) (*FilesWatch, error) {
	var fw = new(FilesWatch)
	return fw, fw.init(file)
}
