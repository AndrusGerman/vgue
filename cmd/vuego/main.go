package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/AndrusGerman/vgue/cmd/vuego/build"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/libs"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/newapp"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/server"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgocomponents"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/vgoflags"
	"gitlab.com/AndrusGerman/vgue/cmd/vuego/wasm"
)

func init() {
	flag.Parse()
}

func main() {
	fmt.Println("VGUE: init 🔥")
	var selec = vgoflags.GetPlain(0)
	if selec == "" {
		notCommandMessage()
	}

	switch selec {
	case "serve":
		server.Start(vgoflags.GetPlain(1))
	case "wasm":
		wasm.Start(vgoflags.GetPlain(1))
	case "new":
		newapp.Start(vgoflags.GetPlain(1))
	case "build":
		build.Build(vgoflags.GetPlain(1))
	case "libs":
		libs.Start(vgoflags.GetPlain(1))
	case "start":
		build.BuildNow()
		server.Start(vgoflags.GetPlain(1))
	case "gc":
		vgocomponents.Start(vgoflags.GetPlain(1))
	default:
		fmt.Println("VGUE: '" + flag.Arg(0) + "' Command Not found 🥺")
	}
}

func notCommandMessage() {
	errMessage := "Please Used vuego command, example:"
	errMessage += "\n + 'vuego serve' - Simple http server (VueRouter)"
	errMessage += "\n + 'vuego new'   - Generate basic (Hello App)"
	errMessage += "\n -	'-d'          - Download Libs"
	errMessage += "\n + 'vuego build' - Build app (Compile app)"
	errMessage += "\n + 'vuego start' - Build and Server (VueRouter)"
	errMessage += "\n -	'-w'          - Watch files change, rebuild"
	errMessage += "\n + 'vuego wasm'  - Get wasm_exec file"
	errMessage += "\n + 'vuego libs'  - Get http://*.js file"
	errMessage += "\n -	'-add'        - Add in index.html"
	errMessage += "\n + 'vuego gc'    - Generate components Files"
	fmt.Println(errMessage)
	os.Exit(1)
}
