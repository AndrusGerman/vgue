package vgue

import (
	"syscall/js"

	"gitlab.com/AndrusGerman/vgue/vuegotools"
)

// Component Model for Vue
type Component struct {
	// Instance : for call this component
	instance string
	// Template : string data
	Template     string
	LazyTemplate func() []byte
	// Primitive data
	Primitive
}

// Set VueComponet Component
// instance: Example 'user-componet'
func (ctx *Component) Set(instance string) (js.Value, error) {
	ctx.instance = instance
	// Create Basic Dt
	var dat = ctx.GetDataObject()

	// Get Vue
	vg, err := vuegotools.GetVue()
	if err != nil {
		return js.Undefined(), err
	}
	// Set componet
	return vg.Call("component", ctx.instance, dat), nil
}

// GetDataObject Convert Component data in Object
func (ctx *Component) GetDataObject(vparams ...Params) js.Value {
	// Add Params
	ctx.Params = append(ctx.Params, Params{Name: "template", Value: ctx.Template})
	return ctx.getParseData(vparams...)
}
