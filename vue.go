package vgue

import (
	"errors"
	"syscall/js"

	"gitlab.com/AndrusGerman/vgue/vuegotools"
)

// Vue Model, Main component model
type Vue struct {
	// El: id element html 'main'
	El string
	// Primitive Data for Vue
	Primitive
}

// Params is custom params in newVue Object
// Parameter model vue
type Params struct {
	// Name : Name identifying the parameter
	Name string
	// Value : js.Value or primitive Go
	Value interface{}
}

// Start Vue Component
// Start the main component
func (ctx *Vue) Start(vparams ...Params) (js.Value, error) {
	// Verifications
	if ctx.El == "" {
		return js.Undefined(), errors.New("'El' cannot be empty, id is null")
	}
	// Mount param
	vparams = append(vparams, Params{Name: "el", Value: ctx.El})
	// Create Basic Dt
	var dat = ctx.getParseData(vparams...)

	// Get Vue
	vg, err := vuegotools.GetVue()
	if err != nil {
		return js.Undefined(), err
	}
	return vg.New(dat), nil
}

// StartWait : Start vue and wait infinitely
func (ctx *Vue) StartWait(vparams ...Params) error {
	var canal = make(chan error)
	go func() {
		_, err := ctx.Start(vparams...)
		if err != nil {
			canal <- err
		}
	}()
	return <-canal
}
