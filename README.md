# VueGo: Go for Vue.js! | Go 1.16

This is a WebAssembly **Vue.js** wrapper written in **Go**.

## How to run VueGOCLI Example ~ Linux:

* Download 'VueGO CLI' ~ Linux:  
 `go install gitlab.com/AndrusGerman/vgue/cmd/vuego@latest`
* Create App Example:  
 `"$(go env GOPATH)/bin/vuego" new MyApp`
* Run Created App 'Enter in "MyApp" folder and run':  
 `"$(go env GOPATH)/bin/vuego" serve`
* Open in your browser: [http://localhost:3001](http://localhost:3001/)

## How to run VueGOCLI Example ~ Windows:

* Download 'VueGO CLI' ~ Windows:  
 `go install gitlab.com/AndrusGerman/vgue/cmd/vuego@latest`
* Create App Example:  
 `vuego new MyApp`
* Run Created App 'Enter in "MyApp" folder and run':  
 `vuego serve`
* Open in your browser: [http://localhost:3001](http://localhost:3001/)

# How to  Create basic app:
 ### Creating the code for app (Hello VueGo):
 * **Go: (main.go)**
``` go
package main

import (
	"gitlab.com/AndrusGerman/vgue"
)

func  main() {
	// Create Basic app
	app  := vuego.Vue {
		El: "#app",
		Primitive: vuego.Primitive{
			// Defined vars For Vue
			Data: map[string]interface{}{
				"message": "HelloWord VueGo",
			},
		},
	} 
	// Start and Wait
	app.StartWait()
}
```
* **Html: (index.html)**
``` html
<!DOCTYPE  html>
<html  lang="en">
<head>
	<meta  charset="UTF-8">
	<meta  name="viewport"  content="width=device-width, initial-scale=1.0">
	<meta  http-equiv="X-UA-Compatible"  content="ie=edge">
	<title>Hello VueGo</title>
	<!-- VueJs import --> 
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<!-- Wasm Runtime forGo --> 
	<script src="./wasm_exec.js"></script>
</head>
	<body>
		<!-- Div App Content -->
		<div id="app">
			{{ message }}
		</div>
		<!-- Call VueGo App -->
		<script>
            const go = new Go();
            WebAssembly.instantiateStreaming(fetch('./main.wasm'),go.importObject).then(v =>go.run(v.instance));
        </script>
	</body>
</html>
```
* **wasm_exec (wasm_exec.js)**:
	 * Execute in console: 
  ``cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .``  
  > When this finalize command generates the desired file automatically
### Build and Run:
* Build:
	* Open folder in console and execute
	`GOOS=js GOARCH=wasm go build -o main.wasm`
* Run:
	* Open folder and install 'serve' for create basic server:
	`go get -v github.com/mattn/serve`
	* And Run basic server (**Unix**):
	`"$(go env GOPATH)/bin/serve"`
	* And Run basic server (**Windows**):
	`"serve"`

# Components

Components are reusable Vue instances with a name.  in this case, `<button-counter>`
* **Go (main.go)**:
``` go
app  := vuego.Vue{El:"#demo-component"}

// Define Component
component  := vuego.Component{
	Template: `<button v-on:click="count++">You clicked me {{ count }} times.</button>`,
	Primitive: vuego.Primitive{
		// Define Var components
		Data: map[string]interface{}{"count": 0},
	},
}

// Set in vue
component.Set("button-counter")

// Run app
app.StartWait()
```
# Vue use

* **Go (main.go)**:
``` go
import (
	vtool "gitlab.com/AndrusGerman/vgue/vuegotools"
)

func main() {
	var materialvue = js.Global().Get("VueMaterial").Get("default")
	vtool.VueUse(materialvue)
}
```
* **HTML (index.html)**:
``` html
<div  id="demo-component">
	<button-counter></button-counter>
	<button-counter></button-counter>
</div>
```

# Methods
* **HTML (methods.html)**
``` html
<div  id="demo-methods">
	<p>Alert Simple: </p>
	<button  v-on:click="alertFunc('Hi')">Alert Simple</button>

	<br>

	<p>Alert Input: "{{messagealert}}"</p>
	<input  placeholder="Input For: Alert Input"  v-model="messagealert">
	<button  v-on:click="alertInputFunc">Alert Input</button>
</div>
```
* **Go (methods.go)**
``` go
package main

import (
	"syscall/js"
	"gitlab.com/AndrusGerman/vgue"
)

func  main() {
	// Create Basic app
	app  := vuego.Vue{
		El: "#demo-methods",
		Primitive: vuego.Primitive{
		// Methods
		Methods: map[string]js.Func{
			// Alert Simple Function
			"alertFunc": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
				js.Global().Call("alert", args[0])
				return  nil
			}),
			// Alert Input
			"alertInputFunc": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
				js.Global().Call("alert", this.Get("messagealert")) // Alert input example
				return  nil
			}),
		},

		// Data
		Data: map[string]interface{}{
			"messagealert": "",
			},
		},
	}

	// Start and Wait
	app.StartWait()
}
```

# Watch
* **HTML (watch.html)**
``` html
<div  id="demo-watch">
	<input  placeholder="Input Log"  v-model="textinput">
</div>
```
* **Go (watch.go)**
``` go
package main

import (
	"syscall/js"
	"gitlab.com/AndrusGerman/vgue"
)

func  main() {
	// Create Basic app
	app  := vuego.Vue{
		El: "#demo-watch",
		Primitive: vuego.Primitive{
			// Watch Methods
			Watch: map[string]js.Func{
			// Watch for 'textinput'
			"textinput": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
				newText  := args[0].String()
				oldText  := args[1].String()
				fmt.Printf("Old: '%s', New: '%s'\n", oldText, newText)
				return  nil
				}),
			},
			// Data
			Data: map[string]interface{}{"textinput": ""},
		},
	}
	// Start and Wait
	app.StartWait()
}
```

# Params:
Params is the ability to add custom parameters when creating a Vue instance.
##  Vuetify on VueGO thanks to params:
### HTML =
* Add CSS Vuetify **HEAD HTML (index.html)**
``` html
<head>
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
```
* Add JS Vuetify **BODY HTML (index.html)** after importing vue and before VueGo:
```html
 <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
```
* Add Code App in **BODY HTML (index.html)**
``` html
<div  id="demo-vuetify-params">
	<v-app>
		<v-content>
			<v-container>Hello VueGo (Vuetify)</v-container>
		</v-content>
	</v-app>
</div>
```
### GO =
* Create basic app:
 ``` go
 // Create Basic app
app  := vuego.Vue{El: "#demo-vuetify-params"}
```
* Add Param Vuetify in **Start** or **StartWait**:
``` go
// Start and Wait

app.StartWait(
	vuego.Params{Name: "vuetify", Value:js.Global().Get("Vuetify").New()},
)
```
### All Code (Go,HTML) Vuetify Params:
* **GO**
``` go
package main

import (
	"syscall/js"
	vuego "gitlab.com/AndrusGerman/vgue"
)

func  main() {
	var vuetify = vuego.Params{Name: "vuetify", Value: js.Global().Get("Vuetify").New()}
	// Create Basic app
	app  := vuego.Vue{El: "#demo-vuetify-params"}
	// Start and Wait
	app.StartWait(
		vuetify,
	)
	// or routes
	r.MountWait("#demo-vuetify-params", vuetify)
}
```
* **HTML**
``` html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Hello VueGo</title>
<!-- Styles -->
<link  href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900"  rel="stylesheet">
<link  href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css"  rel="stylesheet">
<link  href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css"  rel="stylesheet">
<meta  name="viewport"  content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
<!-- Scripts-->
<script  src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script  src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
<script  src="./wasm_exec.js"></script>
</head>
<body>
	<div id="demo-vuetify-params">
		<v-app>
			<v-main>
				<v-container>Hello VueGo (Vuetify)</v-container>
			</v-main>
		</v-app>
	</div>
	<script>
	const go = new Go();
	WebAssembly.instantiateStreaming(fetch('./main.wasm'), go.importObject).then(v => go.run(v.instance));
	</script>
</body>
</html>
```

